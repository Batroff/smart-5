# Service status codes
SVC_RES_OK  = 0
SVC_RES_ERR = 1

# Services
SVC_THERM = 0
SVC_WET   = 1
SVC_LIGHT = 2

# Common headers
HEADER_X_SVC = 'X-Svc-ID'

# interval in seconds
METRICS_INTERVAL = 30
