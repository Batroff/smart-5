from lib.const import *


def make_res(status=None, data=None):
    if status is None:
        status = SVC_RES_OK

    if data is None:
        status = SVC_RES_ERR

    return {'status': status, 'data': data}