import json
import logging
import os
import time

import requests
import random

from lib.const import *

GW_HOST = 'gateway'
GW_PORT = 8080
if (HOST := os.environ.get('GW_HOST')) is not None:
    GW_HOST = HOST

if (PORT := os.environ.get('GW_PORT')) is not None:
    GW_PORT = PORT


def do_useful_thing():
    s = requests.Session()
    light_enabled = {
        'LIGHT_1': random.choice([False, True]),
        'LIGHT_2': random.choice([False, True]),
        'LIGHT_3': random.choice([False, True])
    }

    data = {
        'action': light_enabled,
    }
    headers = {
        HEADER_X_SVC: str(SVC_LIGHT),
        'Content-Type': 'application/json'
    }
    res = s.post(f'http://{GW_HOST}:{GW_PORT}/api/light',
                 data=json.dumps(data),
                 headers=headers,
                 timeout=5)
    logging.info('response: %d %s', res.status_code, res.content)


while True:
    try:
        do_useful_thing()
    except requests.RequestException as e:
        print(f'request error: {e}')

    time.sleep(METRICS_INTERVAL)
