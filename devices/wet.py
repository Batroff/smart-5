import json
import os
import random
import time

import requests

from lib.const import *

GW_HOST = 'gateway'
GW_PORT = 8080
if (HOST := os.environ.get('GW_HOST')) is not None:
    GW_HOST = HOST

if (PORT := os.environ.get('GW_PORT')) is not None:
    GW_PORT = PORT


def do_useful_thing():
    s = requests.Session()
    cur_wet = random.randint(0, 100)

    data = {
        'action': f'set {cur_wet}',
    }
    headers = {
        HEADER_X_SVC: str(SVC_WET),
        'Content-Type': 'application/json'
    }
    res = s.post(f'http://{GW_HOST}:{GW_PORT}/api/wet',
                 data=json.dumps(data),
                 headers=headers,
                 timeout=5)

    print('response:', res.status_code, res.content)


while True:
    try:
        do_useful_thing()
    except requests.RequestException as e:
        print(f'request error: {e}')

    time.sleep(METRICS_INTERVAL)
