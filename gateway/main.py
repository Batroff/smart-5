import os

import requests
from flask import Flask, request, Response, Request

from lib import utils, const

app = Flask(__name__)


def gateway_ctrl(req: Request) -> Response:
    s = requests.Session()

    app.logger.info('proxying to controller from %s', req.host)
    res = s.request(req.method, 'http://controller:8888/api/action',
                    headers=dict(req.headers),
                    data=req.data)

    return Response(res.content, res.status_code, dict(res.headers))


@app.route("/api/<path:path>", methods=['GET', 'POST'])
def gateway(path: str):
    if path == 'alive':
        return utils.make_res(status=const.SVC_RES_OK, data='alive')
    elif path in ['therm', 'wet', 'light']:
        return gateway_ctrl(request)
    return utils.make_res(status=const.SVC_RES_ERR, data='unexpected route')


if __name__ == '__main__':
    HOST = os.environ.get('HOST')
    if HOST is None:
        HOST = '0.0.0.0'

    PORT = os.environ.get('PORT')
    if PORT is None:
        PORT = '8888'

    app.run(host=HOST, port=int(PORT))
