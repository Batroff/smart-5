import os

from flask import Flask, request

from lib import const, utils

app = Flask(__name__)


@app.route("/api/action", methods=['POST'])
def controller_action():
    svc = request.headers.get(const.HEADER_X_SVC, None)
    if svc is None:
        return utils.make_res(status=const.SVC_RES_ERR, data='X-Svc header empty'), 400

    if request.content_type != 'application/json':
        return utils.make_res(status=const.SVC_RES_ERR, data='unexpected Content-Type'), 400

    action = request.json['action']
    if action is None:
        return utils.make_res(status=const.SVC_RES_ERR, data=f'{svc} empty action'), 400

    return utils.make_res(status=const.SVC_RES_OK, data=f'{svc}: {action} res')


@app.route("/api/alive", methods=['GET'])
def controller_alive():
    return utils.make_res(status=const.SVC_RES_OK, data='alive')


if __name__ == '__main__':
    HOST = os.environ.get('HOST')
    if HOST is None:
        HOST = '0.0.0.0'

    PORT = os.environ.get('PORT')
    if PORT is None:
        PORT = '8888'

    app.run(host=HOST, port=int(PORT))
